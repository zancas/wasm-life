import { Universe, Cell } from "wasm-game-of-life";
import { memory } from "wasm-game-of-life/wasm_game_of_life_bg"; 

const CELL_LENGTH = 15; // px
const GRIDLINE_WIDTH = 1;
const AXISWIDTH = (CELL_LENGTH + GRIDLINE_WIDTH)
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#0fbf0f";
const UNI_WIDTH = 20;
const UNI_HEIGHT = 20;
const UNI_SIZE = UNI_WIDTH * UNI_HEIGHT;


const universe = Universe.new(UNI_HEIGHT, UNI_WIDTH);
const canvas = document.getElementById("game-of-life-canvas");
const ctx = canvas.getContext('2d');
//console.log("canvas.ImageData.height: ", ctx.getImageData().height);
//console.log("canvas.height pre assignment: ", canvas.height);
canvas.height = AXISWIDTH * UNI_HEIGHT + GRIDLINE_WIDTH;
//console.log("canvas.height post assignment: ", canvas.height);
canvas.width = AXISWIDTH * UNI_WIDTH + GRIDLINE_WIDTH;

//Half time square at corner of universe
universe.populate(19,19);
universe.populate(0,0);
universe.populate(0,19);
universe.populate(19,0);

//Half time square
universe.populate(17,17);
universe.populate(17,18);
universe.populate(18,17);
universe.populate(18,18);

//Glider
const horizontal_shift = 6;
const vertical_shift = 12;
universe.populate(5 + vertical_shift, 2 + horizontal_shift);
universe.populate(5 + vertical_shift, 3 + horizontal_shift);
universe.populate(5 + vertical_shift, 4 + horizontal_shift);
universe.populate(6 + vertical_shift, 4 + horizontal_shift);
universe.populate(6 + vertical_shift, 4 + horizontal_shift);
universe.populate(7 + vertical_shift, 3 + horizontal_shift);

const getIndex = (row, column) => {
    return row * UNI_WIDTH + column;
};
let pause_toggle = false;

const drawGrid = () => {
    ctx.beginPath();
    
    //Vertical Lines
    for (let i = 0; i <= UNI_WIDTH; i++) {
        ctx.moveTo(i * AXISWIDTH, 0);
        ctx.lineTo(i * AXISWIDTH, canvas.height);
    }
     
    // Horizontal lines.
    for (let j = 0; j <= UNI_HEIGHT; j++) {
        ctx.moveTo(0, j * AXISWIDTH);
        ctx.lineTo(canvas.width, j * AXISWIDTH);
    }

    ctx.stroke();
};

const drawCrosshairs = (xcoord, ycoord) => {
    ctx.beginPath();
    ctx.strokeStyle = "#FF0000";
    // Horizontal
    ctx.moveTo(0, ycoord);
    ctx.lineTo(canvas.width, ycoord);
    ctx.moveTo(xcoord, 0);
    ctx.lineTo(xcoord, canvas.height);
    ctx.stroke();
    ctx.strokeStyle = "#000000";
}

const drawCell = (row, col) => {
    ctx.beginPath();
    ctx.fillRect(
    col * AXISWIDTH + GRIDLINE_WIDTH,
    row * AXISWIDTH + GRIDLINE_WIDTH,
    CELL_LENGTH,
    CELL_LENGTH);
    ctx.stroke();
}

const drawCells = () => {
    const cellsPtr = universe.cells_ptr();
    const cells = new Uint8Array(memory.buffer, cellsPtr, UNI_SIZE);

    for (let row = 0; row < UNI_HEIGHT; row++) {
        for (let col = 0; col < UNI_WIDTH; col++) {
            const idx = getIndex(row, col);
            ctx.fillStyle = cells[idx] === Cell.Dead
                ? DEAD_COLOR
                : ALIVE_COLOR;
            drawCell(row, col, idx);
        }
    }
};

const toggle_life = (click_event) => {
    // There are issues with this when the canvas has been scaled down.
    // How do I find the right scaling factor?
    const xcoord = click_event.pageX - canvas.offsetLeft;
    const ycoord = click_event.pageY - canvas.offsetTop;
    drawCrosshairs(xcoord, ycoord);

    const column_remainder = xcoord % AXISWIDTH;
    const row_remainder = ycoord % AXISWIDTH;
    const column_quotient = Math.floor(xcoord/AXISWIDTH);
    const row_quotient = Math.floor(ycoord/AXISWIDTH);
    console.log("column_quotient: ", column_quotient);
    console.log("row_quotient: ", row_quotient);
    const cell_index = getIndex(row_quotient, column_quotient);
    const cellsPtr = universe.cells_ptr();
    const cells = new Uint8Array(memory.buffer, cellsPtr, UNI_SIZE);
    if ( cells[cell_index] === Cell.Dead ) {
        cells[cell_index] = Cell.Alive;
        ctx.fillStyle = "#ff0000";
    } else {
        cells[cell_index] = Cell.Dead;
        ctx.fillStyle = DEAD_COLOR;
    }
    console.log("about to call drawCell with: ", row_quotient, column_quotient);
    drawCell(row_quotient, column_quotient);
    drawCrosshairs(xcoord, ycoord);
};

canvas.addEventListener('click', toggle_life);

const spaceup = (argevent) => {
    if( argevent.code === 'Space' ) {
        if (pause_toggle) {
            pause_toggle = false;
        } else {
            pause_toggle = true;
        }
    }
}

document.addEventListener('keyup', spaceup);

const renderLoop = () => {
    if (pause_toggle) {
        universe.tick();
        drawGrid();
        drawCells();
    }
    requestAnimationFrame(forsleep);
};

const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
};

async function forsleep() {
    await sleep(200);
    renderLoop();
};

drawGrid();
drawCells();
forsleep();
