#![allow(dead_code)]
#![allow(unused_variables)]

#[macro_use]
extern crate log;

const DEBUG: bool = false;
mod utils;
pub use utils::set_panic_hook;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Cell {
    Dead = 0,
    Alive = 1,
}

#[wasm_bindgen]
#[derive(Debug)]
pub struct Universe {
    width: i32,
    height: i32,
    cells: Vec<Cell>,
}

use std::fmt;
impl fmt::Display for Universe {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for line in self.cells[..].chunks(self.width as usize) {
            for cell in line {
                let symbol = if *cell == Cell::Alive { '◼' } else { '◻' };
                write!(f, "{}", symbol)?;
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}

#[wasm_bindgen]
impl Universe {
    pub fn new(width: i32, height: i32) -> Self {
        let cells = vec![Cell::Dead; (width * height) as usize];
        Universe {
            width,
            height,
            cells,
        }
    }

    /*
    /// Set the width of the universe.
    ///
    /// Resets all cells to the dead state.
    pub fn set_width(&mut self, width: u32) {
        self.width = width as i32;
        self.cells = (0..width * self.height as u32)
            .map(|_i| Cell::Dead)
            .collect();
    }

    /// Set the height of the universe.
    ///
    /// Resets all cells to the dead state.
    pub fn set_height(&mut self, height: u32) {
        self.height = height as i32;
        self.cells = (0..self.width as u32 * height)
            .map(|_i| Cell::Dead)
            .collect();
    }
    */

    pub fn set(&mut self, xcoord: i32, ycoord: i32) {
        let localindex = self.get_index(xcoord, ycoord);
        match self.cells[localindex] {
            Cell::Alive => self.cells[localindex] = Cell::Dead,
            Cell::Dead => self.cells[localindex] = Cell::Alive,
        }
    }

    pub fn width(&self) -> i32 {
        self.width
    }

    pub fn height(&self) -> i32 {
        self.height
    }

    pub fn cells_ptr(&self) -> *const Cell {
        self.cells.as_ptr()
    }

    pub fn populate(&mut self, living_x_coordinate: i32, living_y_coordinate: i32) {
        let index = self.get_index(living_x_coordinate, living_y_coordinate) as usize;
        self.cells[index] = Cell::Alive;
    }

    pub fn set_cells(&mut self, xcoords: &[i32], ycoords: &[i32]) {
        for (x, y) in xcoords.iter().zip(ycoords.iter()) {
            let index = self.get_index(*x, *y) as usize;
            self.cells[index] = Cell::Alive;
        }
    }

    pub fn tick(&mut self) {
        let mut next = self.cells.clone();
        for index in 0..self.cells.len() {
            match self.live_neighbor_count(index) {
                0 | 1 => next[index] = Cell::Dead,
                2 => next[index] = self.cells[index],
                3 => next[index] = Cell::Alive,
                4..=8 => next[index] = Cell::Dead,
                _ => panic!("Impossible number of neighbors!"),
            }
            if next[index] != self.cells[index] && DEBUG {
                let (x, y) = self.get_row_column(index);
                //log!("Transition at ({}, {})!", x, y);
            }
        }
        self.cells = next;
    }

    pub fn random_population(&mut self) {
        use js_sys::Math;
        for index in 0..self.cells.len() {
            if Math::random() > 0.5 {
                self.cells[index] = Cell::Alive;
            } else {
                self.cells[index] = Cell::Dead;
            }
        }
    }

    fn get_index(&self, row: i32, column: i32) -> usize {
        ((self.width * row) + column) as usize
    }

    fn get_row_column(&self, index: usize) -> (usize, usize) {
        div_rem(index, self.width as usize)
    }

    fn get_neighbors(&self, index: usize) -> [usize; 8] {
        let index_row = index as i32 / self.width;
        let mut results: Vec<usize> = vec![];
        let columnoffsets: [i32; 3] = [-1, 0, 1];
        let rowoffsets: [i32; 3] = [-1, 0, 1];
        for rowoffset in &rowoffsets {
            let current_row = (rowoffset + index_row as i32)
                .checked_rem_euclid((self.height) as i32)
                .unwrap() as i32;
            for i in 0..3 {
                let coloffset = columnoffsets[i]; // If both coloffset and rowoffset are 0, skip.
                if !((rowoffset == &coloffset) && (*rowoffset == 0)) {
                    let ni = (coloffset + index as i32)
                        .checked_rem_euclid(self.width as i32)
                        .unwrap() as i32
                        + current_row * self.width;
                    results.push(ni as usize);
                }
            }
        }
        use std::convert::TryInto;
        results[..].try_into().expect("8 usizes")
    }

    fn live_neighbor_count(&self, index: usize) -> u8 {
        let mut alive = 0;
        let neighbor_indices = self.get_neighbors(index);
        for index in &neighbor_indices {
            if self.cells[*index] == Cell::Alive {
                alive = alive + 1;
            }
        }
        alive
    }

    pub fn render(&self) -> String {
        self.to_string()
    }
}

fn div_rem(x: usize, d: usize) -> (usize, usize) {
    (x / d, x % d)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_div_rem() {
        assert_eq!(div_rem(5, 5), (1, 0));
        assert_eq!(div_rem(6, 5), (1, 1));
        assert_eq!(div_rem(7, 5), (1, 2));
        assert_eq!(div_rem(8, 6), (1, 2));
        assert_eq!(div_rem(6, 4), (1, 2));
        let (mut blocks, rem) = div_rem(8 * 9, 32);
        println!("{}", (rem > 0) as usize - (true as usize));
        println!("{}", blocks);
        blocks += (rem > 0) as usize;
        println!("{}", blocks);
        let x = fixedbitset::FixedBitSet::with_capacity(400);
        println!("{:?}", x);
        println!("{:?}", x.len());
    }

    /*
    #[test]
    pub fn test_random_population() {
        //Tautology test
        let mut u = Universe::new(20, 20);
        u.random_population();
        //println!("{}", u);
    }*/

    #[test]
    fn get_index() {
        let u = Universe::new(4, 5);
        assert_eq!(u.get_index(0i32, 0i32), 0);
        assert_eq!(u.get_index(1i32, 2i32), 6);
    }

    #[test]
    fn test_tick() {
        let coordinates = vec![(0i32, 2i32), (1i32, 2i32), (2i32, 2i32)];
        let future_coordinates = vec![(1i32, 1i32), (1i32, 2i32), (1i32, 3i32)];
        let mut u = Universe::new(4, 5);
        let mut future_living_indices: Vec<usize> = future_coordinates
            .iter()
            .map(|x| u.get_index(x.0, x.1))
            .collect();
        assert_eq!(future_living_indices, [5, 6, 7]);
        u.populate(coordinates[0].0, coordinates[0].1);
        u.populate(coordinates[1].0, coordinates[1].1);
        u.populate(coordinates[2].0, coordinates[2].1);
        u.tick();
        for index in 0..u.cells.len() {
            if !future_living_indices.is_empty() && (future_living_indices[0] == index) {
                future_living_indices.remove(0);
                assert_eq!(u.cells[index], Cell::Alive);
            } else {
                assert_eq!(u.cells[index], Cell::Dead);
            }
        }
        let mut u2 = Universe::new(4, 4);
        let init_coords = vec![(1, 1), (1, 2), (2, 1), (2, 2)];
        u2.populate(1, 1);
        u2.populate(1, 2);
        u2.populate(2, 1);
        u2.populate(2, 2);
        let init_indices: Vec<usize> = init_coords.iter().map(|x| u2.get_index(x.0, x.1)).collect();
        let mut position = 0;
        for index in 0..u2.cells.len() {
            if u2.cells[index] == Cell::Alive {
                assert_eq!(index, init_indices[position]);
                position = position + 1;
            }
        }
        u2.tick();
        let mut position = 0;
        for index in 0..u2.cells.len() {
            if u2.cells[index] == Cell::Alive {
                assert_eq!(index, init_indices[position]);
                position = position + 1;
            }
        }
        u2.tick();
        let mut position = 0;
        for index in 0..u2.cells.len() {
            if u2.cells[index] == Cell::Alive {
                assert_eq!(index, init_indices[position]);
                position = position + 1;
            }
        }
    }

    #[test]
    fn test_live_neighbor_count() {
        let mut u = Universe::new(4, 5);
        u.populate(0, 2);
        u.populate(1, 2);
        u.populate(2, 2);
        assert_eq!(u.live_neighbor_count(0), 0);
        assert_eq!(u.live_neighbor_count(1), 2);
        assert_eq!(u.live_neighbor_count(2), 1);
        assert_eq!(u.live_neighbor_count(3), 2);
        assert_eq!(u.live_neighbor_count(4), 0);
        assert_eq!(u.live_neighbor_count(5), 3);
        assert_eq!(u.live_neighbor_count(6), 2);
        assert_eq!(u.live_neighbor_count(7), 3);
        assert_eq!(u.live_neighbor_count(8), 0);
        assert_eq!(u.live_neighbor_count(9), 2);
        assert_eq!(u.live_neighbor_count(10), 1);
        assert_eq!(u.live_neighbor_count(11), 2);
        assert_eq!(u.live_neighbor_count(12), 0);
        assert_eq!(u.live_neighbor_count(13), 1);
        assert_eq!(u.live_neighbor_count(14), 1);
        assert_eq!(u.live_neighbor_count(15), 1);
        assert_eq!(u.live_neighbor_count(16), 0);
        assert_eq!(u.live_neighbor_count(17), 1);
        assert_eq!(u.live_neighbor_count(18), 1);
        assert_eq!(u.live_neighbor_count(19), 1);
    }

    #[test]
    fn test_get_neigbors() {
        let u = Universe::new(4, 3);
        assert_eq!(u.get_neighbors(5), [0, 1, 2, 4, 6, 8, 9, 10]);
        assert_eq!(u.get_neighbors(3), [10, 11, 8, 2, 0, 6, 7, 4]);
        assert_eq!(u.get_neighbors(0), [11, 8, 9, 3, 1, 7, 4, 5]);
        let u2 = Universe::new(4, 5);
        assert_eq!(u2.get_neighbors(0), [19, 16, 17, 3, 1, 7, 4, 5]);
        assert_eq!(u2.get_neighbors(19), [14, 15, 12, 18, 16, 2, 3, 0]);
    }

    #[test]
    fn test_populate() {
        let mut u = Universe::new(4, 3);
        u.populate(0, 2);
        u.populate(1, 2);
        u.populate(2, 2);
        assert_eq!(
            u.cells,
            [
                Cell::Dead,
                Cell::Dead,
                Cell::Alive,
                Cell::Dead,
                Cell::Dead,
                Cell::Dead,
                Cell::Alive,
                Cell::Dead,
                Cell::Dead,
                Cell::Dead,
                Cell::Alive,
                Cell::Dead
            ]
        );
        let mut u2 = Universe::new(2, 2);
        //u2.populate(&vec![(0i32, 0i32), (1i32, 1i32)]);
        u2.populate(0, 0);
        u2.populate(1, 1);
        assert_eq!(u2.cells, [Cell::Alive, Cell::Dead, Cell::Dead, Cell::Alive]);
    }

    #[test]
    fn test_display() {
        let mut u = Universe::new(4, 4);
        let init_coords = vec![(1, 1), (1, 2), (2, 1), (2, 2)];
        u.populate(1, 1);
        u.populate(1, 2);
        u.populate(2, 1);
        u.populate(2, 2);
        println!("{}", u);
        assert_eq!(format!("{}", u), "◻◻◻◻\n◻◼◼◻\n◻◼◼◻\n◻◻◻◻\n");
    }
}
